<head>
  <link rel="stylesheet" href="http://www.w3schools.com/lib/w3.css">
  <?php
  include_once 'includes/register.inc.php';
   ?>
</head>


<?php
$mysqli=new mysqli("localhost","root","","project");


if (login_check($mysqli) == false){ ?>
  <script type="text/JavaScript" src="js/sha512.js"></script>
  <script type="text/JavaScript" src="js/forms.js"></script>
<div class="w3-top">
<nav class="navbar navbar-inverse" ><ul class="w3-navbar w3-border w3-light-grey">
    <li><a class="w3-green" href="?controller=pages&action=home">Home</a></li>
    <li class="w3-right"><a class="w3-red " style="cursor:pointer" onclick="document.getElementById('id02').style.display='block'">Signup</a></li>
    <li class="w3-right"><a class="w3-green" style="cursor:pointer" onclick="document.getElementById('id01').style.display='block'">Login</a></li>
    <li class="w3-right"><input type="text" class="w3-input" placeholder="Search.."></li>
  </ul>
</nav>
</div>
<?php }



else {?>
<div class="w3-top">
<nav class="navbar navbar-inverse" ><ul class="w3-navbar w3-border w3-light-grey">
    <li><a class="w3-green" href="?controller=pages&action=home">Home</a></li>
    <li><a href="?controller=pages&action=add">Add</a></li>
    <li><a href="#" ><span class="glyphicon "><span style="font-size:13">&#9812;</span></span><?php echo htmlentities($_SESSION['username']); ?></a></li>
    <li class="w3-right"><a class="w3-red " href="includes/logout.php" style="cursor:pointer" >Logout</a></li>
    <li class="w3-right"><input type="text" class="w3-input" placeholder="Search.."></li>
  </ul>
</nav>
</div>
<?php } ?>



<div id="id01" class="w3-modal">
    <div class="w3-modal-content w3-card-8 w3-animate-zoom" style="max-width:600px">
      <header class="w3-container w3-teal">
        <span onclick="document.getElementById('id01').style.display='none'"
        class="w3-closebtn">&times;</span>
        <h2>Login</h2>
      </header>

      <form action="includes/process_login.php" method="post" name="login_form">
        <span class="glyphicon glyphicon-lock" style="position:absolute;font-size:25;top:134;left:65;z-index:2;width:41px;height:41px;"></span>
        <span class="glyphicon glyphicon-user" style="position:absolute;font-size:25;top:93;left:65;z-index:2;width:41px;height:41px;"></span>
        <input placeholder="Email" class="login-input" name="email" type="text" style="top:83;">
        <input placeholder="Password" class="login-input" name="password" type="password" style="top:126;">
          <input type="submit" value="Login" onclick="formhash(this.form,this.form.password);" class="loginbutton" />
      </form>


    </div>
  </div>
  <div id="id02" class="w3-modal">
      <div class="w3-modal-content w3-card-8 w3-animate-zoom" style="max-width:600px">
        <header class="w3-container w3-teal">
        <span onclick="document.getElementById('id02').style.display='none'"
        class="w3-closebtn">&times;</span>
        <h2>Signup</h2>
      </header>

      <form action="<?php echo esc_url($_SERVER['REQUEST_URI']); ?>" method="post" name="registration_form">
              <i class="glyphicon glyphicon-ok" id='check1' style="color:green;position:absolute;top:28;right:100;" ></i>
              <i class="glyphicon glyphicon-ok" id='check2' style="color:green;position:absolute;top:88;right:100;" ></i>
              <i class="glyphicon glyphicon-ok" id='check3' style="color:green;position:absolute;top:148;right:100;" ></i>
              <i class="glyphicon glyphicon-ok" id='check4' style="color:green;position:absolute;top:208;right:100;" ></i>
              <font class="form" style="top:22px;">Username</font>
              <input type='text' name='username' id='username' class="divbox" placeholder="Username" style="top:20px;" /><br>
              <label style="top:53px;left:250px;" hidden="true"id="1">Contain only digits,upper and lowercase letter</label>
              <font class="form" style="top:82px;">Email</font>
              <input type="text" name="email" id="email" class="divbox" placeholder="Email"style="top:80px;"/><br>
              <label style="top:113px;left:250px;" hidden="true" id='2'>Emails must have a valid email format</label>
              <font class="form" style="top:142px;">Password</font>
              <input type="password" name="password" id="password" class="divbox" placeholder="Password"style="top:140px;"/><br>
              <label style="top:173px;left:250px;" hidden="true" id='3'>At least 6 characters and have (a-z) , (A - Z) , (0 - 9)</label>
              <font class="form" style="top:202px;">Confirm Password</font>
              <input type="password" name="confirmpwd" id="confirmpwd" class="divbox" placeholder="Comfirm password"style="top:200px;" /><br>
              <label style="top:233px;left:250px;" hidden="true" id='4' >Must match password exactly</label>
            <input type="button" value="Register" class="button1" style="bottom:20px;right:130px;" onclick="return regformhash(this.form,this.form.username,this.form.email,this.form.password,this.form.confirmpwd);" />
        </div>
        </div>
      </form>
      </div>
    </div>
