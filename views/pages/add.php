<body>
    <div style="margin-top: 40px;">
        <form class="w3-container w3-card-4 w3-light-grey" method="post" action="?controller=process&action=add_post">
          <h2>Add</h2>
        <p>
          <label>Name</label>
          <input class="w3-input w3-border w3-round-xxlarge" name="name" type="text">
        </p>
        <p>
          <label>Detail</label>
          <input class="w3-input w3-border w3-round-xxlarge" name="detail" type="text">
        </p>
        <p>
          <label>Price</label>
          <input class="w3-input w3-border w3-round-xxlarge" name="price" type="text">
        </p>
        <p>
          <label>Type</label>
          <input class="w3-input w3-border w3-round-xxlarge" name="type" type="text">
        </p>

        <button class="w3-btn-block w3-green w3-section w3-padding" type="submit">Add</button>

        </form>
    </div>
</body>
