<header class="w3-display-container w3-content w3-wide" style="max-width:1500px;" >
  <img class="w3-image" src="pic/nxe0j9ej3MfOXVbWkHL-o.jpg" alt="Architecture" width="1500" height="800">
  <div class="w3-display-middle w3-margin-top w3-center">
    <h1 class="w3-xxlarge w3-text-white"><span class="w3-padding w3-black w3-opacity-min"><b>SHOP</b></span> <span class="w3-hide-small w3-text-light-grey">Architects</span></h1>
  </div>
</header>
<body>
<div class="w3-main w3-content w3-padding" style="max-width:1200px;margin-top:100px">

  <!-- First Photo Grid-->
  <div class="w3-row-padding w3-padding-16 w3-center" >
      
    <?php foreach($posts as $post){ ?>
       <div class="w3-quarter">
          <img src="pic/46522_344270122334569_1894143786_n.jpg" style="width:100%">
          <h3><?php echo $post->title ?></h3>
          <p><?php echo $post->detail ?></p>
          <p><?php echo $post->price ?></p>
          <a href="?controller=pages&action=product&id=<?php echo $post->id ?>">See more</a>
        </div>
    <?php } ?>
      
  </div>
    
</div>





</div>
</body>
