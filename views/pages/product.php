<!DOCTYPE html>
<html>
  <head>
    <link href="views/pages/star/rating.css" rel="stylesheet" type="text/css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script type="text/javascript" src="views/pages/star/rating.js"></script>
    <script >
    $(function() {
        $("#rating_star").codexworld_rating_widget({
            starLength: '5',
            initialValue: '',
            callbackFunctionName: 'processRating',
            imageDirectory: 'views/pages/star/images/',
            inputAttr: 'postID',
            inputAttr2: 'userid'
        });
    });

    function processRating(val, attrVal , attrVal2){
        $.ajax({
            type: 'POST',
            url: 'views/pages/star/rating.php',
            data: 'postID='+attrVal+'&ratingPoints='+val+'&userid='+attrVal2,
            dataType: 'json',
            success : function(data) {
                if (data.status == 'ok') {
                    alert('You have rated '+val+' to CodexWorld');
                    $('#avgrat').text(data.average_rating);
                    $('#totalrat').text(data.rating_number);
                      $('#test').css('visibility','hidden');

                }else{
                    alert('Some problem occured, please try again.');
                }
            }
        });
    }
    </script>
    <meta charset="utf-8">
    <title></title>
  </head>
  <body style="margin-top:10%;">
    <div class="w3-panel">

  <div class="w3-container w3-half">
    <center><p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
      Product:<?php echo $post->title; ?></p></center>
    <!-- show picture-->
    <img src="<?php echo $img_src ?>" style=" margin-left:20%; width:70%;cursor:zoom-in"
 onclick="document.getElementById('modal01').style.display='block'">
 <div style="margin-left:30%;margin-top:20px">
   <?php include_once '/star/dbConfig.php';
 $id=$_GET['id'];
 //Fetch rating deatails from database
 $query = "SELECT rating_number, FORMAT((total_points / rating_number),1) as average_rating FROM post_rating WHERE post_id = $id AND status = 1";
 $result = $db->query($query);
 $ratingRow = $result->fetch_assoc();

$mysqli=new mysqli("localhost","root","","project");
 if (login_check($mysqli) == true){
 $user__id=$_SESSION['user_id'];

 $prevRatingQuery = "SELECT * FROM rating_table WHERE post_id = $id AND user_id =$user__id ";
 $prevRatingResult = $db->query($prevRatingQuery);
 if(!$prevRatingResult->num_rows > 0):

?><div id="test" onclick="return confirm('Are you sure to vote ?')">
  <input name="rating" value="0" id="rating_star" type="hidden" postID="<?php echo $_GET['id']; ?>" userid="<?php echo htmlentities($_SESSION['user_id']); ?>" />
</div>
<?php

 endif;

 ///////////////////////////////////////////////////
 }
  ?>

 <div class="overall-rating">
     (Average Rating <span id="avgrat"><?php echo $ratingRow['average_rating']; ?></span> Based on <span id="totalrat"><?php echo $ratingRow['rating_number']; ?></span>  rating)
 </div>
</div>
<!-- ถ้าคลิ๊กจะขยายขนาด-->
<div id="modal01" class="w3-modal" onclick="this.style.display='none'">
 <span class="w3-closebtn w3-hover-red w3-container w3-padding-16 w3-display-topright">&times;</span>
 <div class="w3-modal-content w3-animate-zoom">

   <img src="<?php echo $img_src ?>" style="width:100%">

 </div>
</div>
</div>

<?php
$mysqli=new mysqli("localhost","root","","project");
?>
<div class="w3-container w3-half">
  <!-- ส่วนของdetail-->
  <div style="margin-left:10%;" class="w3-container w3-section w3-border w3-border-purple w3-hover-border-blue">
     <div class="w3-padding-48"><p>By:<a onclick="document.getElementById('proflie').style.display='block'"><?php echo $post->a_id; ?></a></p><p>Detail:<?php echo $post->detail; ?></p><p>Sell at:<?php echo $post->time; ?></p><p><h4>Price:<?php echo $post->price; ?></h4></p></div>
   </div>

    <?php if (login_check($mysqli) == true){ ?>

<div class="w3-container w3-third">

</div>

<div  class="w3-container w3-third">
<!-- ปุ่มmessage-->
<p><button class="w3-btn w3-green w3-xlarge w3-padding-large" onclick="document.getElementById('blmessage').style.display='block'">Message</button></p>
</div>
<div  class="w3-container w3-third">
<!-- ปุ่มซื้อ-->
<p><button class="w3-btn w3-green w3-xlarge w3-padding-large" onclick="document.getElementById('blbuy').style.display='block'">Buy</button></p>
</div>

<?php } ?>
</div>
</div>
<!-- ส่วนของcomment-->
<div class="w3-panel">

  <div class="w3-panel w3-round-jumbo w3-teal w3-large ">
    comment
  </div>
  <blockquote>
<?php

  foreach($comments as $c){

      echo '<div class ="w3-panel w3-section w3-pale-green w3-leftbar w3-border-green ">'.$c->comment.'<div class ="w3-container w3-border-top w3-border-green">- '.$c->userid.'</div>'.'</div>';

  }

?>
</blockquote>
<!-- write  comment here-->
<?php if (login_check($mysqli) == true){ ?>
  <br><p>Post a comment: </p>

      <form method="post" action="?controller=process&action=add_comment&postid=<?php echo $_GET['id'] ?>&userid=<?php echo $_SESSION['id_username'] ?>">

          <input type="text" name="comm"><br>

          <button type="submit">Comment</button>

      </form><?php } ?>
</div>
<!-- pop-up conferm-->
<div id="blbuy" class="w3-modal">
  <div class="w3-modal-content w3-card-8 w3-animate-zoom" style="max-width:600px">
    <header class="w3-container w3-teal">
    <span onclick="document.getElementById('blbuy').style.display='none'"
    class="w3-closebtn">&times;</span>
    <h2>Confirm</h2>
  </header>
  <img src="<?php echo $img_src ?>" style="margin-top:5%;margin-left:20%; width:55%;cursor:zoom-in"
onclick="document.getElementById('modal01').style.display='block'">
    <form class="w3-container" action="form.asp">
      <div class="w3-section">
        <label><b>Name</b></label>
        <input class="w3-input w3-border w3-margin-bottom" type="text" placeholder="Enter Name" name="name" required>
        <label><b>Address</b></label>
        <input class="w3-input w3-border" type="text" placeholder="Enter Address" name="Adss" required>
        <label><b>Tell</b></label>
        <input class="w3-input w3-border" type="text" placeholder="Enter Phone Number" name="Tell_p" required>
        <label><b>Time</b></label>
        <input class="w3-input w3-border" type="text" placeholder="Enter Time" name="time" required>
        <button class="w3-btn-block w3-green w3-section w3-padding" type="submit">Confirm</button>

      </div>
    </form>
  </div>
</div>
<!-- pop-up send message-->
<div id="blmessage" class="w3-modal">
    <div class="w3-modal-content w3-card-8 w3-animate-zoom" style="max-width:600px">
      <header class="w3-container w3-teal">
      <span onclick="document.getElementById('blmessage').style.display='none'"
      class="w3-closebtn">&times;</span>
      <h2>Send Message</h2>
    </header>
      <form class="w3-container" method="post" action="?controller=process&action=send_message&rid=<?php echo $_GET['a_id'] ?>">
        <div class="w3-section">

          <label><b>Title</b></label>
          <input class="w3-input w3-border w3-margin-bottom" type="text" placeholder="Enter Title" name="title" required>

          <label><b>Message</b></label>
          <textarea style="resize:none" class="w3-input w3-border w3-margin-bottom" name="message" cols="50" rows="10"  placeholder="Enter Message"></textarea>

          <button class="w3-btn-block w3-green w3-section w3-padding" type="submit">Send</button>

        </div>
      </form>
    </div>
  </div>


</body>
<div id="proflie" class="w3-modal">
    <div class="w3-modal-content w3-card-8 w3-animate-zoom" style="max-width:600px">

      <div class="w3-center"><br>
        <span onclick="document.getElementById('proflie').style.display='none'" class="w3-closebtn w3-hover-red w3-container w3-padding-8 w3-display-topright" title="Close Modal">&times;</span>
        <img src="img_avatar4.png" alt="Avatar" style="width:30%" class="w3-circle w3-margin-top">
      </div>



      <div class="w3-container w3-border-top w3-padding-16 w3-light-grey">



        <?php
        $follower = $_SESSION['user_id'];
        $following = $post->a_id;
        if($follower != $following){
$mysqli=new mysqli("localhost","root","","project");

        $prevRatingQuery2 = "SELECT * FROM follow WHERE follower = $follower AND following =$following ";
        $prevRatingResult = $mysqli->query($prevRatingQuery2);
        if($prevRatingResult->num_rows > 0):
              ?>
              <form action="feed0.php" method="post">
                        <input type="text" name="follower" hidden="true" value="<?php echo $_SESSION['user_id']; ?>">
                        <input type="text" name="following" hidden="true" value="<?php echo $post->a_id ;?>">
                          <input type="text" name="id" hidden="true" value="<?php echo $post->id ;?>">
                <center><input  type="submit" class="w3-btn w3-red" value="UnFollow" /></center>

              <?php
              else:
                //Insert rating data into the database
                ?>
                <form action="feed.php" method="post">
                          <input type="text" name="follower" hidden="true" value="<?php echo $_SESSION['user_id']; ?>">
                          <input type="text" name="following" hidden="true" value="<?php echo $post->a_id ;?>">
                            <input type="text" name="id" hidden="true" value="<?php echo $post->id ;?>">
                  <center><input  type="submit" class="w3-btn w3-red" value="Follow" /></center>
                <?php
            endif;}
         ?>



      </div>

    </div>
  </div>
</html>
