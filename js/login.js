$(document).ready(function(){
    $("#check1").hide();
    $("#check2").hide();
    $("#check3").hide();
    $("#check4").hide();
    $("#username").focus(function(){
        $("#1").show();

    });
    $("#username").focusout(function(){
        $("#1").hide();
    });
    $("#email").focus(function(){
        $("#2").show();
    });
    $("#email").focusout(function(){
        $("#2").hide();
    });
    $("#password").focus(function(){
        $("#3").show();
    });
    $("#password").focusout(function(){
        $("#3").hide();
    });
    $("#confirmpwd").focus(function(){
        $("#4").show();
        var a = document.getElementById('confirmpwd').value ;
        var b = document.getElementById('password').value ;
        if(a == b && a.length > 0){
          $("#4").css("color","green");
          $("#4").text("Match password exactly");

        }else{
            $("#4").css("color","#FF6600");
            $("#4").text("Must match password exactly");
        }
    });
    $("#confirmpwd").focusout(function(){
        $("#4").hide();
    });
    $("#confirmpwd").keyup(function(){
      var a = document.getElementById('confirmpwd').value ;
      var b = document.getElementById('password').value ;
      if(a == b && a.length > 0){
        $("#4").css("color","green");
        $("#4").text("Match password exactly");
        $("#check4").show();
      }else{
          $("#4").css("color","#FF6600");
          $("#4").text("Must match password exactly");
          $("#check4").hide();
      }
    });

    $("#password").keyup(function(){
      var password = document.getElementById('password').value ;
        if(password.length> 5 && password.match(/^([a-zA-Z0-9\_-])+$/i) && password.match(/([a-z])/) && password.match(/([A-Z])/) && password.match(/([0-9])/)){
                $("#3").css("color","green");
                $("#3").text("password is correct");
                $("#check3").show();
          }  else{
                $("#3").css("color","#FF6600");
                $("#3").text("At least 6 characters and have (a-z) , (A - Z) , (0 - 9)");
                $("#check3").hide();
            }
    });
    $("#email").keyup(function(){
        var mail  =document.getElementById('email').value ;
	      var Email=/^([a-zA-Z0-9.-_]+)@([a-zA-Z0-9]+)\.([a-zA-Z0-9.]{2,5})$/;
	      if(Email.test(mail))
	         {
             $("#2").css("color","green");
             $("#2").text("Email is correct");
             $("#check2").show();
	          }
	            else
	                {
                    $("#2").css("color","#FF6600");
                    $("#2").text("Emails must have a valid email format");
                    $("#check2").hide();
	                    }
    });

    $("#username").keyup(function(){
      var username = document.getElementById('username').value ;
        if(username.match(/^([a-zA-Z0-9\-])+$/i)){
                $("#1").css("color","green");
                $("#1").text("username is correct");
                $("#check1").show();
          }  else{
                $("#1").css("color","#FF6600");
                $("#1").text("Contain only digits,upper and lowercase letter");
                $("#check1").hide();
            }
    });
});
