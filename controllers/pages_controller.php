<?php

    class PagesController{
        
        public function home(){
            
            $posts = Post::all();
            
            require_once('views/pages/home.php');
        }
        
        public function add(){
            
            require_once('views/pages/add.php');
        }
        
        public function product(){
            $comments = Comment::comm_list($_GET['id']);
            
            require_once('views/pages/product.php');
        }
    
    }

?>