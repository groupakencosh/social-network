<?php

    class ProcessController{
        
    public function add_post(){
        
        $title = $_POST['name'];
        $userid = 1;
        $time = 0000;
        $time = date("Y-m-d H-i-s");
        $type = $_POST['type'];
        $rating = 5;
        $detail = $_POST['detail'];
        $price = $_POST['price'];
    
        /*if(!isset($author) || empty($content)){
            return call('pages', 'error');
        }*/
        
        $db = Database::getInstance();
    
        $req = $db->prepare("INSERT INTO post(title, userid, time, typeid, rating, detail, pice) VALUES (:title, :userid, :time, :type, :rating, :detail, :price)");
            
        $req->bindParam(':title', $title);
        $req->bindParam(':userid', $userid);
        $req->bindParam(':time', $time);
        $req->bindParam(':type', $type);
        $req->bindParam(':rating', $rating);
        $req->bindParam(':detail', $detail);
        $req->bindParam(':price', $price);

        $req->execute();
        
        header("Location: ?controller=pages&action=home");
    }
        
        public function add_comment(){
            
            $pid = $_GET['postid'];
            $author = $_GET['userid'];
            $comm = $_POST['comm'];
            
            if(empty($pid) || empty($comm)){
                return call('pages', 'error');
            }
            
            $db = Database::getInstance(); 

            $req = $db->prepare("INSERT INTO comments(postid, comment,  userid) VALUES (:pid, :comment, :userid)");

            $req->bindParam(':pid', $pid);
            $req->bindParam(':userid', $author);
            $req->bindParam(':comment', $comm);
            
            $req->execute();

            header("Location: ?controller=pages&action=product&id=".$pid);
        }
    
}

?>