<?php

    function call($controller, $action){
        
        require_once('controllers/'.$controller.'_controller.php');
        
        switch($controller){

            case 'pages':
                require_once('models/post.php');
                require_once('models/comment.php');
                $controller = new PagesController();
            break;
            
            case 'process':
                require_once('models/post.php');
                require_once('database.php');
                $controller = new ProcessController();
            break;
                
        }
        
        $controller->{$action}();
        
    }

    $controllers = array('pages'=>['home', 'add', 'product'],
                         'process'=>['add_post', 'add_comment']);

    if(array_key_exists($controller, $controllers)){
        if(in_array($action, $controllers[$controller])){
            call($controller, $action);
        }
        else{
            //call('pages', 'error');
        }
    }
    else{
        //call('pages', 'error');
    }
?>