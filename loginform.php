<?php
include_once 'includes/register.inc.php';
include_once 'includes/functions.php';

?>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script type="text/JavaScript" src="js/sha512.js"></script>
<script type="text/JavaScript" src="js/forms.js"></script>
<script type="text/JavaScript" src="js/login.js"></script>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

<?php
  if (!empty($error_msg)) {
    echo $error_msg;
  }
?>

<head>
</head>
<link rel="stylesheet" href="css/style.css"></link>

<div class="logmain">
  <div class="signup">
    <div class="main">
<form action="<?php echo esc_url($_SERVER['REQUEST_URI']); ?>" method="post" name="registration_form">
        <i class="glyphicon glyphicon-ok" id='check1' style="color:green;position:absolute;top:28;right:100;" ></i>
        <i class="glyphicon glyphicon-ok" id='check2' style="color:green;position:absolute;top:88;right:100;" ></i>
        <i class="glyphicon glyphicon-ok" id='check3' style="color:green;position:absolute;top:148;right:100;" ></i>
        <i class="glyphicon glyphicon-ok" id='check4' style="color:green;position:absolute;top:208;right:100;" ></i>
        <font class="form" style="top:22px;">Username</font>
        <input type='text' name='username' id='username' class="divbox" placeholder="Username" style="top:20px;" /><br>
        <label style="top:53px;left:250px;" hidden="true"id="1">Contain only digits,upper and lowercase letter</label>
        <font class="form" style="top:82px;">Email</font>
        <input type="text" name="email" id="email" class="divbox" placeholder="Email"style="top:80px;"/><br>
        <label style="top:113px;left:250px;" hidden="true" id='2'>Emails must have a valid email format</label>
        <font class="form" style="top:142px;">Password</font>
        <input type="password" name="password" id="password" class="divbox" placeholder="Password"style="top:140px;"/><br>
        <label style="top:173px;left:250px;" hidden="true" id='3'>At least 6 characters and have (a-z) , (A - Z) , (0 - 9)</label>
        <font class="form" style="top:202px;">Confirm Password</font>
        <input type="password" name="confirmpwd" id="confirmpwd" class="divbox" placeholder="Comfirm password"style="top:200px;" /><br>
        <label style="top:233px;left:250px;" hidden="true" id='4' >Must match password exactly</label>
      <input type="button" value="Register" class="button1" style="bottom:20px;right:130px;" onclick="return regformhash(this.form,this.form.username,this.form.email,this.form.password,this.form.confirmpwd);" />
  </div>
  </div>
</form>
  <div class="login">
    <?php
    if (isset($_GET['error'])) {
      if($_GET['error'] == 1){
        echo '<p id="error" style="top:120;right:38;position:absolute;color:red;">* Email or password is invalid Please Input again.</p>';
    }else if ($_GET['error'] == 2) {
        echo '<p id="error" style="top:120;left:120;position:absolute;color:red;">* Please login first </p>';
    }
  }else if (isset($_GET['register'])) {
  if ($_GET['register'] == 1){
      echo '<script>';
      echo 'alert("Register Success You can login now");';
      echo '</script>';
    }else if($_GET['register'] == 2){
        echo '<script>';
        echo 'alert("Username already exists");';
        echo '</script>';
      }}
     ?>
      <div class="logincontent">
          <form action="includes/process_login.php" method="post" name="login_form">
            <span class="glyphicon glyphicon-lock" style="position:absolute;font-size:25;top:134;left:65;z-index:2;width:41px;height:41px;"></span>
            <span class="glyphicon glyphicon-user" style="position:absolute;font-size:25;top:93;left:65;z-index:2;width:41px;height:41px;"></span>
            <input placeholder="Email" class="login-input" name="email" type="text" style="top:83;">
            <input placeholder="Password" class="login-input" name="password" type="password" style="top:126;">
              <input type="submit" value="Login" onclick="formhash(this.form,this.form.password);" class="loginbutton" />
          </form>
      </div>
  </div>
  <div class="or">
    or
  </div>

</div>
