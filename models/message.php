<?php

    class Message{
        
        public $id;
        public $sender;
        public $receiver;
        public $content;
        
        public function __construct($id, $sender, $receiver, $content){
            $this->id = $id;
            $this->sender = $sender;
            $this->receiver = $receiver;
            $this->content = $content;
        }
        
        public function m_list($id){
            
            //$id = current user's id
            
            $list = [];
            
            $db = Database::getInstance();
            
            $req = $db->prepare('SELECT * FROM messages WHERE receiver ='.$id);
            
            $req->execute();
            
            foreach($req->fetchAll() as $message){
                $list[] = new Message($message['id'], $message['sender'], $message['receiver'], $message['content']);
            }
            
            return $list;
            
        }
        
    }

?>