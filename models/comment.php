<?php

    class Comment{
        
        public $id;
        public $postid;
        public $author;
        public $message;
        
        public function __construct($id, $postid, $comment, $userid){
            $this->id = $id;
            $this->postid = $postid;
            $this->comment = $comment;
            $this->userid = $userid;
        }
        
        public function comm_list($id){
            
            $list = [];
            
            $db = Database::getInstance();
            
            $req = $db->prepare('SELECT * FROM comments WHERE postid = '.$id);
            
            $req->execute();
            
            foreach($req->fetchAll() as $comment){
                $list[] = new Comment($comment['id'], $comment['postid'], $comment['comment'], $comment['userid']);
            }
            
            return $list;
            
        }
        
    }

?>