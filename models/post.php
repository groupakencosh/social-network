<?php

    class Post{
        
        public $id;
        public $title;
        public $a_id;
        public $time;
        public $type;
        public $rating;
        public $detail;
        public $price;
        
        public function __construct($id, $title, $a_id, $time, $type, $rating, $detail, $price){
            $this->id = $id;
            $this->title = $title;
            $this->a_id = $a_id;
            $this->time = $time;
            $this->type = $type;
            $this->rating = $rating;
            $this->detail = $detail;
            $this->price = $price;
        }
        
        public static function all(){
            
            $list = [];
            $db = Database::getInstance();
            $req = $db->query('SELECT * FROM post ORDER BY id DESC');
            
            foreach($req->fetchAll() as $post){
                
                $list[] = new Post($post['id'], $post['title'], $post['userid'], $post['time'], $post['typeid'], $post['rating'], $post['detail'], $post['pice'], $post['img']);
                
            }
            
            return $list;
            
        }
        
        public static function find($id){
            
            $db = Database::getInstance();
            
            $id = intval($id);
            
            $req = $db->prepare('SELECT * FROM posts WHERE id  = :id');
            
            $req->execute(array('id'=>$id));
            $post  = $req->fetch();
            
            return new Post($post['id'], $post['title'], $post['userid'], $post['time'], $post['typeid'], $post['rating'], $post['detail'], $post['pice'], $post['img']);
            
        }
        
        public static function find_cat($id){
            
            $list = [];
            
            $db = Database::getInstance();
            
            $id = intval($id);
            
            $req = $db->query("SELECT * FROM posts WHERE cat =".$id);
            
            foreach($req->fetchAll() as $post){
                
                $list[] = new Post($post['id'], $post['title'], $post['userid'], $post['time'], $post['typeid'], $post['rating'], $post['detail'], $post['pice'], $post['img']);
                
            }
            
            return $list;
            
        }
        
        public static function find_img($id){
            $db = Database::getInstance();
            //Post id
            $id = intval($id);
            
            $req = $db->query("SELECT pic FROM posts WHERE id =".$id);
            
            $post = $req->fetch();
            
            if($post['pic'] == 1){
                
                $req2 = $db->query("SELECT path from images WHERE post_id=".$id);
                $result = $req2->fetch();
                
                return $result['path'];
            
            }
            else{
                return NULL;
            }
            
        }
        
    }

?>