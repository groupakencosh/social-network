<html>

    <?php
        include "includes/db_connect.php";
        include "includes/functions.php";
    
        
        sec_session_start();
    ?>
    <head>
        
        <link rel="stylesheet" href="http://www.w3schools.com/lib/w3.css">
    
    </head>
    
    <?php

        require_once('database.php');

        if(isset($_GET['controller']) && isset($_GET['action'])){
            $controller = $_GET['controller'];
            $action = $_GET['action'];
        }
        else{
            $controller = 'pages';
            $action = 'home';
        }

        require_once('views/layout.php');

    ?>

</html>