<?php
include_once 'includes/register.inc.php';
include_once 'includes/functions.php';
$error = filter_input(INPUT_GET, 'err', $filter = FILTER_SANITIZE_STRING);

if (! $error) {
    $error = 'Oops! An unknown error happened.';
}

sec_session_start();
if(login_check($mysqli) == true) {
        header("Location:protected_page.php");
} else {
        echo 'You are not authorized to access this page, please login.';
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Secure Login: Error</title>
        <link rel="stylesheet" href="styles/main.css" />
    </head>
    <body>
        <h1>There was a problem</h1>
        <p class="error"><?php echo $error; ?></p>
    </body>
</html>
